let alreadyOrdered = false

document.addEventListener('click', e => {
    if (e.target.closest('.button-spid') !== null) {
        orderSPIDProviderList()
        document.getElementById('spid-search-box').focus()
    }
});

const buildList = (providers, dropdown) => {
    dropdown.innerHTML = ''

    const searchBox = document.createElement('input')
    searchBox.id = 'spid-search-box'
    searchBox.setAttribute('style', 'width: 100%; padding: 1rem; color: black')
    searchBox.setAttribute('placeholder', 'Cerca nei provider...')
    dropdown.appendChild(searchBox)

    const div = document.createElement('div')
    div.id = 'spid-providers-list'

    providers.forEach(p => {
        p.element.classList.remove('spid-idp-button-link')
        div.appendChild(p.element)
    })
    dropdown.appendChild(div)

    searchBox.addEventListener('input', e => {
        const value = e.target.value.trim().toLowerCase()
        div.innerHTML = ''
        providers.forEach(p => {
            if (value.length === 0 || p.provider.toLowerCase().indexOf(value) > -1) {
                div.appendChild(p.element)
            }
        })
    })

    searchBox.addEventListener('keyup', e => {
        if (e.keyCode === 13) {
            e.preventDefault();
            
            const list = document.getElementById('spid-providers-list')
            const link = list.firstChild.querySelectorAll('a')
            if (link.length > 0) {
                link[0].firstChild.click()
            }
        }
    })
}

const orderSPIDProviderList = () => {
    if (!alreadyOrdered) {
        let dropdown = document.getElementsByClassName('spid-idp-button-menu')
    
        if (dropdown[0]) {
            dropdown = dropdown[0]
        
            const providers = []
            
            dropdown.childNodes.forEach(e => {
                if (e.classList?.contains('spid-idp-button-link')) {
                    providers.push(
                        {
                            provider: null,
                            element: e
                        }
                    )
                }
            })
        
            document.querySelectorAll('.spid-sr-only').forEach((e, i) => {
                providers[i].provider = e.innerText.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, '')
            })
        
            providers.sort((a, b) => {
                if (a.provider < b.provider) {
                    return -1
                }
                if (a.provider > b.provider) {
                    return 1
                }
                return 0
            })
        
            buildList(providers, dropdown)

            alreadyOrdered = true
        }
    }
}