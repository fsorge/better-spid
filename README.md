![better-spid](https://static.francescosorge.com/file/francesco-sorge/2020-02/software/better-spid/better-spid.webp)

# Better SPID

**Extension for Firefox, Edge and Chrome that sorts SPID providers and adds a handy search box**.



## Get

Better SPID is a publicly and free extension available for Mozilla Firefox, Microsoft Edge and Google Chrome.

You can get it from their official marketplaces:

- [Mozilla Firefox](https://addons.mozilla.org/it/firefox/addon/better-spid/)
- [Microsoft Edge](https://microsoftedge.microsoft.com/addons/detail/better-spid/dggcehmicbaegamglplaokgbcljedfcd)
- [Google Chrome](https://chrome.google.com/webstore/detail/better-spid/nklgfjoiakmfllamoofjhpddilcpgcpm)
